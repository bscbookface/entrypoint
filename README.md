# Endpoint application

Built with Flask and Flask-RESTful. 

Example usage:

    > curl -i -H "Content-Type: application/json" -X POST -d '{"name" : "Ola Nordmann", "jobType" : 3}' 127.0.0.1:5000/job


    HTTP/1.0 201 CREATED
    Content-Type: application/json
    Content-Length: 111
    Server: Werkzeug/0.11.5 Python/2.7.6
    Date: Wed, 13 Apr 2016 11:12:52 GMT

    {
        "jobType" : 3,
        "name": "Ola Nordmann", 
        "stamp": [], 
        "timeStart": "Wed Apr 13 13:12:52 2016"
    }


Installation:

1. Clone from git

2. cd entrypoint-app/ 

3. pip install -r requirements.txt

4. chmod +x entrypoint-app.py

5. ./entrypoint-app.py

