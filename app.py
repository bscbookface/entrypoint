#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Simple web application.
This application built with Flask and RESTful works as an entrypoint to the
architecture. This will accept POST JSON, produce a job and put in on the
message queues with Pika.
"""
# Import
from flask import Flask, request, abort
from flask_restful import Resource, Api
import yaml
import time
import pika
import json

# Define
app = Flask(__name__)
api = Api(app)

# Config setup
with open("config.yml", 'r') as ymlfile:
    cfg = yaml.load(ymlfile)

# Route /job, POST only
class NewJob(Resource):
    """ API resource
    """
    def post(self):
        """	Take request data, puts into an
        object and passes it along to a queue.

        >>> curl -i -H "Content-Type: application/json" -X \
        POST -d '{"name" : "Ola Nordmann", "jobType" : 2}' 127.0.0.1:5000/job
        HTTP/1.0 201 CREATED
      	Content-Type: application/json
      	Content-Length: 119
      	Server: Werkzeug/0.11.5 Python/2.7.6
      	Date: Tue, 12 Apr 2016 16:51:18 GMT

      	{
            "jobType": 2,
      	    "name": "Ola Nordmann",
      	    "stamp": [],
      	    "timeStart": "Tue Apr 12 18:51:18 2016"
      	}

      	"""

        # Not JSON, no name then 400 it.
        if not request.json or not 'name' in request.json:
            abort(400)
        # Create a variable current time.
        localtime = time.ctime(int(time.time()))
        # Get requested data and save it.
        json_dict = request.get_json(force=True)
        # Append stamp array and time variable.
        json_dict.update({"stamp":[], "timeStart":localtime})

        # Pika connection auth
        parameters = pika.ConnectionParameters(host='localhost')
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()

        # Declare the queue and use persistant messages.
        channel.queue_declare(queue='assembly', durable=True)

        # Pika message and delivery
        message = json.dumps(json_dict)
        channel.basic_publish(exchange='', routing_key='assembly',  \
		body=message, properties=pika.BasicProperties(delivery_mode=2, \
		content_type='application/json'))
        print "Sent %r" % message

        #Close connection
        connection.close()
        return json_dict, 201

# Abstract RESTful resources
api.add_resource(NewJob, '/job')


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')

